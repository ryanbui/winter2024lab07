public class SimpleWar{
	public static void main (String [] args){
		Deck gameDeck = new Deck();
		gameDeck.shuffle();
		System.out.println(gameDeck);
		int player1Score = 0;
		int player2Score = 0;
		int numberOfRounds = 1;
		String starLine = "****************************";
		String dottedLine = "------------------------------";
		/*Card testCard1 = gameDeck.drawTopCard();
		Card testCard2 = gameDeck.drawTopCard();
		System.out.println(testCard1);
		System.out.println(testCard2);
		Card testCard3 = gameDeck.drawTopCard();
		System.out.println(testCard3);
		System.out.println("testCard3's score: " + testCard3.calculateScore());
		Card testCard4 = gameDeck.drawTopCard();
		Card testCard5 = gameDeck.drawTopCard();
		System.out.println("testCard4: " +testCard4);
		System.out.println("testCard4's score: " + testCard4.calculateScore());
		System.out.println("testCard5: " +testCard5);
		System.out.println("testCard5's score: " + testCard5.calculateScore());
		if(testCard4.calculateScore() > testCard5.calculateScore()){
			player1Score++;
		} else {
			player2Score++;
		}
		System.out.println("Player 1's Score: " + player1Score  + "\n" + "Player 2's Score: " + player2Score);
		*/
		while(gameDeck.length() > 0){
			System.out.println("Round: " + numberOfRounds + "\n" + dottedLine);
			Card player1Card = gameDeck.drawTopCard();
			double card1Score = player1Card.calculateScore();
			Card player2Card = gameDeck.drawTopCard();
			double card2Score = player2Card.calculateScore();
			System.out.println(player1Card+ "'s score for Player 1: " + card1Score + "\n" +
			player2Card+ "'s score for Player 2: " + card2Score + "\n" + dottedLine);
			if(card1Score > card2Score){
			player1Score++;
			} else {
			player2Score++;
			}
			System.out.println("Player 1's Score: " + player1Score  + "\n" 
			+ "Player 2's Score: " + player2Score + "\n" + starLine);
			numberOfRounds++;
		}
		
		if(player1Score > player2Score){
			System.out.println("Player 1 wins with: " + player1Score + " points");
		} else if(player1Score < player2Score) { 
			System.out.println("Player 2 wins with: " + player2Score + " points");
		} else {
			System.out.println("It is a tie");
		}
	}
}