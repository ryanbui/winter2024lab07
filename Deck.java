import java.util.Random;

public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//Constructor
	public Deck() {
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[this.numberOfCards];
		String [] suits = new String [] {"Hearts", "Diamonds", "Spades", "Clubs"};
		String [] values = new String [] {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		int i = 0;
		for(String suit: suits){
			for(String value: values) {
				cards[i] = new Card(suit, value);
				i++;
			}
		}
	}
	
	//Method to return the length of the deck (numberOfCards)
	public int length(){
		return this.numberOfCards;	
	}
	
	//Method to return the top card of the deck and draw/remove it from the deck 
	public Card drawTopCard() {
		Card topCard = this.cards[this.numberOfCards-1];
		this.numberOfCards--;
		Card [] placeHolder = new Card[this.numberOfCards];
		for(int i = 0; i < placeHolder.length; i++) {
				placeHolder[i] = this.cards[i];
		}
		this.cards = placeHolder;
		return topCard;
	}
	
	//Override the default toString method
	public String toString() {
		String allCardsOnTheDeck = "";
		for(Card c: this.cards) {
			allCardsOnTheDeck += c + "\n";
		}
		return allCardsOnTheDeck;
	}
	
	//Method to shuffle the deck and swapping the positions of the card in the deck
	public void shuffle() {
		for(int i = 0; i < this.cards.length-1; i++){
			int randomPos = rng.nextInt(this.cards.length);
			Card currentCard = cards[i];
			Card swap = cards[randomPos];
			cards[i] = swap;
			cards[randomPos] = currentCard;
		}
	}
}