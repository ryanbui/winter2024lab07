public class Card {
	private String suit;
	private String value;
	
	//Constructor
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}	
	
	//Getter methods 
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	
	//Override the default toString method
	public String toString(){
		return this.value + " of " + this.suit;
	}
	
	/* 
	This method will return a double, where the number BEFORE 
	the decimal is the rank of the card (i.e., a rank 2 card
	will be 2.0, rank 3 is 3.0... 10 is 10.0, Jack is 11.0, etc). 
	We then reserve the numbers AFTER the decimal to be the suit encoded
	(i.e., Hearts is 0.4, spades 0.3, diamonds 0.2, clubs 0.1)
	*/
	public double calculateScore(){
		double suitScore = 0;
		double rankScore = 0;
		if(this.value.equals("Ace")){
			rankScore = 1.0;
		} else if(this.value.equals("Two")){
			rankScore = 2.0;
		} else if(this.value.equals("Three")){
			rankScore = 3.0;
		} else if(this.value.equals("Four")){
			rankScore = 4.0;
		} else if(this.value.equals("Five")){
			rankScore = 5.0;
		} else if(this.value.equals("Six")){
			rankScore = 6.0;
		} else if(this.value.equals("Seven")){
			rankScore = 7.0;
		} else if(this.value.equals("Eight")){
			rankScore = 8.0;
		} else if(this.value.equals("Nine")){
			rankScore = 9.0;
		} else if(this.value.equals("Ten")){
			rankScore = 10.0;
		} else if(this.value.equals("Jack")){
			rankScore = 11.0;
		} else if(this.value.equals("Queen")){
			rankScore = 12.0;
		} else {
			rankScore = 13.0;
		} 
		
		if(this.suit.equals("Hearts")){
		suitScore = 0.4;
		} else if(this.suit.equals("Spades")){
		suitScore = 0.3;
		} else if(this.suit.equals("Diamonds")){
		suitScore = 0.2;
		} else {
		suitScore = 0.1;
		}
		return rankScore + suitScore;
	}
}